# Generate Git SSH key

[Master-Branch](https://gitlab.com/XavierEduardo99/generate-git-ssh-key/-/tree/main)

This bash script will help the user to create and set up an SSH key to use with Gitlab.

## Dependencies:
In order to run this project you will need the following packages
* ssh and bash

## Usage guide: 

+ To generate a new SSH and import it to your Gitlab account, run:
```sh
$ ./generate-my-git-key.sh
```

## Contact with the developer
| Contact | Link |
| ------ | ------ |
| My Mail | xaviervintimilla@yahoo.es |
